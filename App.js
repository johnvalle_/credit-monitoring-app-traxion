import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation'

import LoginScreen from '@screens/LoginScreen';
import RegisterScreen from '@screens/RegisterScreen';
import HomeScreen from '@screens/HomeScreen';
import ApplyLoanScreen from '@screens/ApplyLoanScreen';
import EditScreen from '@screens/EditScreen';
import PaymentScreen from '@screens/PaymentScreen';
import PaymentDetailsScreen from '@screens/PaymentDetailsScreen';

const MainNavigator = createStackNavigator({

  //MAIN PAGES
  Login: { screen: LoginScreen, navigationOptions: { gesturesEnabled: false } },
  Register: { screen: RegisterScreen, navigationOptions: { gesturesEnabled: false } },
  Home : {screen : HomeScreen, navigationOptions: {gesturesEnabled : false}},
  ApplyLoan : {screen : ApplyLoanScreen, navigationOptions: {gesturesEnabled : false}},
  Edit : {screen : EditScreen, navigationOptions: {gesturesEnabled : false}},
  Payment : {screen : PaymentScreen, navigationOptions : {gesturesEnabled : false}},
  PaymentDetails : {screen : PaymentDetailsScreen, navigationOptions : {gesturesEnabled : false}},
  headerMode: 'screen',
  }, {
    initialRouteName: 'Login'
  });

const App = createAppContainer(MainNavigator);

export default App;
