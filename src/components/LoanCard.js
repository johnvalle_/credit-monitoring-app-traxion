import React from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
import {Card, CardTitle, CardAction, CardContent, CardButton} from 'react-native-material-cards';
import { withNavigation } from 'react-navigation';
import Loader from '@components/Loader';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class LoanCard extends React.Component{
    _handleApply(id, name, desc, price){
        this.props.navigation.navigate('ApplyLoan', {
            item_id : id,
            name : name,
            description : desc,
            price : price,
            user_id : this.props.user_id,
            user_name : this.props.user_name,
            contact : this.props.contact,
            email : this.props.email,
            accessToken : this.props.accessToken
        })
    }
    render(){
        const results = this.props.data;
        let data;
        if(results != null){
            data = results.map((val) => 
            <Card style={styles.loanCard} key={val.id}>
                <View style={{justifyContent: 'flex-start', marginLeft : 20, }}>
                    <Text style={styles.loanCardPrice}>PHP {val.price}</Text>
                    <Text style={styles.loanCardTitle}>{val.name}</Text>
                </View>
                <CardContent text={val.description} style={styles.loanCardContent} />
                <CardAction 
                    separator={false} 
                    inColumn={false}
                    style = {{flexDirection : 'row', justifyContent : 'center'}}
                >
                    <CardButton
                        onPress={() => this._handleApply(val.id, val.name, val.description, val.price)}
                        title="Apply"
                        color={'#FFFF'}
                        style={styles.loanCardButton}
                    />
                </CardAction>
            </Card>
        )
        }else{
            data = <View style={{alignItems: 'center', justifyContent: 'center'}}></View>
        }
        
        return(
            data
        )
    }
}

const styles = StyleSheet.create({
    loanCard : {
        width : width*.9,
        borderRadius : 20,
        elevation : 4,
        marginTop : 30,
    },
    loanCardContent : {
        marginTop : 0,
        marginBottom : 0,
        paddingTop : 20,
        paddingBottom : 20
    },
    loanCardButton : {
        backgroundColor : '#5271ff',
        borderRadius : 20,
        height : 40,
        width : 130,
    },
    loanCardTitle : {
        color : '#5271ff',
        marginBottom : 10,
        fontSize : 18,
        marginTop : 10,
        padding : 0

    },
    loanCardPrice : {
        color : '#93d265',
        marginTop : 10,
        fontSize : 24,
        fontWeight : 'bold',
        padding : 0
    },
    loanCardContent : {
        borderBottomWidth: .75, 
        borderBottomColor : '#CCCC', 
        borderTopWidth: .75, 
        borderTopColor : '#CCCC',
        padding : 10,
    }
})

export default withNavigation(LoanCard);