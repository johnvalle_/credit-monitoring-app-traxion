import React from "react";
import { ActivityIndicator } from "react-native";

const Loader = () => {
  return (
    <ActivityIndicator
      size="large"
      color="#93d265"
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    />
  );
};

export default Loader;
