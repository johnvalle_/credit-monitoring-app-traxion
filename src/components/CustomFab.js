import React from 'react';
import { Fab, Button, Icon } from 'native-base';
import { withNavigation } from 'react-navigation';

class CustomFab extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            active : false,
        }
    }
    _handleHome(){
        this.props.navigation.navigate('Home');
        this._closeFab();
    }
    _handleLogOut(){
        this.props.navigation.navigate('Login');
        this._closeFab();
    }
    _handleEdit(){
        this.props.navigation.navigate('Edit', {
            firstName : this.props.firstName,
            lastName : this.props.lastName,
            contactNo : this.props.contactNo,
            userId : this.props.id
        });
        this._closeFab();
    }
    _handlePayment(){
        this.props.navigation.navigate('Payment', {

        });
        this._closeFab();
    }
    _closeFab(){
        this.setState({
            active : false
        })
    }
    render(){
        return(
            <Fab
                active={this.state.active}
                direction="up"
                containerStyle={{ }}
                style={{ backgroundColor: '#93d265' }}
                position="bottomRight"
                onPress={() => this.setState({ active: !this.state.active })}>
                <Icon type="Feather" name="menu" style={{color : 'white'}} />
                <Button onPress={() => this._handleLogOut()} style={{ backgroundColor: '#394A95' }}>
                    <Icon type="Feather" name="log-out" style={{color : 'white'}} />
                </Button>
                <Button onPress={() => this._handlePayment()} style={{ backgroundColor: '#5767B0' }}>
                    <Icon type="Feather" name="credit-card" style={{color : 'white'}} />
                </Button>
                <Button onPress={() => this._handleEdit()} style={{ backgroundColor: '#8391CF' }}>
                    <Icon type="Feather" name="edit" style={{color : 'white'}} />
                </Button>
                <Button onPress={() => this._handleHome()} style={{ backgroundColor: '#A4B0E9' }}>
                    <Icon type="Feather" name="home" style={{color : 'white'}} />
                </Button>
            </Fab>
        
        )
    }

}

export default withNavigation(CustomFab);