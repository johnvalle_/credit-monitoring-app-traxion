import React, { Component } from 'react';
import { Text, View } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import style from '../assets/styles/style';
import Modal from 'react-native-modal';

class ModalAlert extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            visibleModal : false,
        }
    }
    render(){
        this.setState({
            visibleModal : this.props.visibility
        })
        return(
            <Modal
            isVisible ={this.state.visibleModal} 
            style={styles.bottomModal}
            onBackdropPress={() => this.setState({ visibleModal : false})}
            >
                <View style={styles.modalContent}>
                {this.props.toShow == 'empty' ? (
                    <View>
                        <Text style={[styles.modalContentTitle, style.warning]}>Uh oh. Some fields are empty.</Text>
                        <Text style={styles.modalContentSubtitle}>Please fill all fields with information.</Text>
                    </View>
                ) : (
                    <View>
                        <Text style={[styles.modalContentTitle, style.danger]}>Registration Failed.</Text>
                        <Text style={styles.modalContentSubtitle}>User already registered. Please login.</Text>
                    </View>
                )}
                </View>
            </Modal>   

        );
    }
}

const styles = StyleSheet.create({
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 22,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignItems : 'center'
    },
    modalContentSubtitle : {
        color : 'gray'
    }
    
});

export default ModalAlert;