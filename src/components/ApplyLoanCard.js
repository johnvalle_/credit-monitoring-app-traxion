import React from 'react';
import { View, Text, Dimensions, StyleSheet, AsyncStorage } from 'react-native';
import { Divider, Button, ThemeProvider } from 'react-native-elements';
import { Form, Picker, Toast} from 'native-base';
import {withNavigation} from 'react-navigation';
import Api from '@API/initialize';
import Modal from 'react-native-modal';
import style from '@assets/styles/style';

const width = Dimensions.get('window').width;

class ApplyLoanCard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            selected : "9",
            first_name : null,
            last_name : null,
            contact_no : null,
            loading : true,
            user_id : null,
            modalToShow : null,
            visibleModal : false,
        }
    }
    // _handleCancel(){
    //     this.props.navigation.navigate('Home');
    // }
    _backModal(){
        this.props.navigation.navigate('Home');
    }
    _handleConfirm(){
        var params = {
            "user_id" : String(this.state.user_id),
            "item_id" :  String(this.props.item_id),
            "months_to_pay" : this.state.selected,
        }
        let token = this.props.accessToken;
        Api._transact(token, params, (data) => {
            try{
                console.log(data)
                this.setState({
                    visibleModal : !this.state.visibleModal
                })
            }catch(exception){
                console.log(exception)
            }
        })
        console.log(token)  
        console.log(params)
    }
    onValueChange(value) {
        this.setState({
          selected: value
        });
    }
    async _getData(){
        const { navigation } = this.props;
        const token = await AsyncStorage.getItem('userToken');
        const id = this.props.userId;
        this.setState({ loading : true },() => {
            Api._getProfile(id, token, (data)=> {
                try{
                    console.log(data)
                    this.setState({
                        first_name : data.first_name,
                        last_name : data.last_name,
                        contact_no : data.contact_no,
                        email : data.username,
                        loading : false,
                        user_id : data.id
                    })
                }catch(exception){
                    console.log(exception)
                }
            })
        })
        
    }
    componentDidMount(){
        this._isMounted = true;
        if(this._isMounted){
            this._getData();
        }
    }
    render(){
        let res;
        this.state.loading ? (
            res = (
                <Text>Loading...</Text>
            )
        ):(
            res = (
                <React.Fragment>
                    <View style={styles.innerView}>
                        <Text style={styles.title}>
                            {this.props.name}
                        </Text>
                        <Text style={styles.price}>
                            PHP {this.props.price}
                        </Text>
                    </View>
                    <View style={styles.innerView}>
                        <Text style={styles.description}>
                            {this.props.desc}
                        </Text>
                    </View>
                    <View style={styles.innerView}>
                        <Text style={{fontWeight : 'bold', color : 'grey', paddingBottom : 5}}>
                            User Information
                        </Text>
                        <Text style={styles.user_info}>
                            {this.state.email}
                        </Text>
                        <Text style={styles.user_info}>
                            {this.state.first_name + ' '+ this.state.last_name}
                        </Text>
                        <Text style={styles.user_info}>
                            {this.state.contact_no}
                        </Text>
                    </View>
                    <View style={styles.innerView}>
                        <Text style={{fontWeight : 'bold', color : 'grey', paddingBottom : 5}}>
                            Installment loan
                        </Text>
                        <Form>
                            <Picker
                            note
                            mode="dropdown"
                            style={{ width: 160 }}
                            selectedValue={this.state.selected}
                            onValueChange={this.onValueChange.bind(this)}
                            >
                            <Picker.Item label="9 months" value="9" />
                            <Picker.Item label="12 months" value="12" />
                            <Picker.Item label="16 months" value="16" />
                            <Picker.Item label="24 months" value="24" />
                            </Picker>
                        </Form>
                    </View>
                    <View style={styles.buttonView}>
                        {/* <ThemeProvider>
                            <Button 
                                type="outline"
                                onPress={() => {this._handleCancel()}}
                                title="Cancel"
                                buttonStyle={styles.buttonClear}
                                titleStyle = {{color : '#5271FF'}}
                            />
                        </ThemeProvider> */}
                        <Button 
                            type="solid"
                            onPress={() => {this._handleConfirm()}}
                            title="Confirm"
                            buttonStyle={styles.button}
                            raised
                        />
                    </View>
                </React.Fragment>
            )
        )
        return(
           <View styles={styles.wrapper}>
                {res}
                <Modal
                    isVisible ={this.state.visibleModal} 
                    style={styles.bottomModal}
                    onBackdropPress={() => this._backModal()}
                    >
                    <View style={styles.modalContent}>
                        <View>
                            <Text style={[styles.modalContentTitle, style.success]}>Transaction successful.</Text>
                            <Text style={styles.modalContentSubtitle}>You may now see your next payment dates.</Text>
                        </View>
                    </View>
                </Modal>   
           </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper : {
        height : '100%',
        width : width*7,
        flex : 1,
        marginTop : 50
    },
    innerView : {
        paddingTop : 10,
        paddingBottom : 10,
        borderTopWidth : .75,
        borderTopColor : '#CCCC',
        marginLeft : 60,
        marginRight :60
    },
    title : {
        color : '#5271ff',
        fontSize : 18
    },
    price : {
        color : '#93d265',
        fontSize : 24,
        fontWeight : 'bold',
    },
    description : {
        paddingTop : 10,
        paddingBottom : 10,
        color : 'grey',
    },
    user_info : {
        color : 'grey'
    },
    buttonView : {
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent: 'center',
        marginTop : 20,
    },
    button : {
        backgroundColor : '#5271ff',
        borderRadius : 20,
        height : 40,
        width : 130,
        padding : 5
    },
    buttonClear : {
        borderRadius : 20,
        height : 40,
        width : 130,
        padding : 5,
        marginRight : 10
    },
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 22,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignSelf : 'center'
    },
    modalContentSubtitle : {
        color : 'gray'
    }
})

export default withNavigation(ApplyLoanCard);