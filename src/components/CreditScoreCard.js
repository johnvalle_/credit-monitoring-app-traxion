import React from 'react';
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';
import {Card, CardTitle, CardAction, CardContent, CardButton} from 'react-native-material-cards';
// import console = require('console');


const width = Dimensions.get('window').width;

const CreditScoreCard = (props) =>{
    const image = 'http://10.199.1.228:8000' + props.image;
    console.log(image)
    return(
        <React.Fragment>
            <Card style={[styles.card, styles.cardTop]} >
                <View style={styles.cardContentWrapper}>
                    <View style={styles.cardContent}>
                        <Image source={{uri : image}} style={styles.imageStyle} />
                    </View>
                    <View style={styles.cardContent}>
                        <Text style={styles.score}>{props.score}</Text>
                        <Text style={styles.scoreDesc}>Your Credit Score</Text>
                    </View>
                </View>
            </Card>
            <Card style={[styles.card, styles.cardBottom]} >
                <View style={styles.cardContentWrapper}>
                    <View style={styles.cardContent}>
                        <Text style={styles.notifDesc}>{props.user_name}</Text>
                        <Text style={styles.notifDescBold}>{props.email}</Text>
                        <Text style={styles.notifDesc}>{props.contact}</Text>
                    </View>
                </View>
            </Card>

        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    card : {
        width : width*.9,
        elevation : 4,
        margin : 0,
        borderWidth : 0,
        padding : 20
    },
    cardTop : {
        borderTopLeftRadius : 20,
        borderTopRightRadius : 20,
        borderBottomLeftRadius : 0,
        borderBottomRightRadius : 0,
        backgroundColor : '#5271ff',
        marginTop : 20,
        marginBottom : 0,
        flexDirection : 'row',
    },
    cardBottom : {
        borderTopLeftRadius : 0,
        borderTopRightRadius : 0,
        borderBottomLeftRadius : 20,
        borderBottomRightRadius : 20,
        backgroundColor : '#FFFF',
        margin : 0,
        marginBottom : 0
    },
    cardContent : {
        flex : 1, 
        alignItems : 'center', 
        justifyContent : 'center', 
        textAlign : 'center'
    },
    imageStyle : {
        width : 60, 
        height: 60, 
        borderRadius:100, 
        borderColor : 'white', 
        borderWidth : 3
    },
    cardContentWrapper : {
        flex : 1, 
        flexDirection : 'row'
    },
    score : {
        color : '#FFFF',
        fontSize : 30,
        fontWeight : 'bold'
    },
    scoreDesc : {
        color : '#FFFF',
        fontSize : 16
    },
    notifDesc : {
        color : 'grey',
        fontSize : 16,
    },
    notifDescBold : {
        color : 'grey',
        fontSize : 16,
        fontWeight : 'bold'
    }
})

export default CreditScoreCard;