'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({
    main : {
        color : '#5271ff'
    },
    accent : {
        color : '#93d265'
    },
    infoOnColored: {
        color : 'white'
    },
    infoOnWhite : {
        color : 'gray'
    },
    success : {
        color : '#99da00'
    },
    warning : {
        color : '#ffdc00'
    },
    danger : {
        color : '#9f0000'
    }
})