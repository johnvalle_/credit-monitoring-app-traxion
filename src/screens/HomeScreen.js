import React from 'react';
import { Text, Button, Icon, ThemeProvider } from 'react-native-elements';
import { Image, BackHandler, AsyncStorage, StyleSheet, View, Dimensions, TouchableOpacity, Platform, ScrollView } from 'react-native';
import JWT from 'expo-jwt';
import style from '@assets/styles/style';
import Modal from 'react-native-modal';
import { ImagePicker } from 'expo';
import Api from '@API/initialize';
import LoanCard from '@components/LoanCard';
import CreditScoreCard from '@components/CreditScoreCard';
import CustomFab from '@components/CustomFab';


const width = Dimensions.get('window').width;
_isMounted = false;
class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loading : true,
            first_name : '',
            last_name : '',
            email : '',
            contact_no : '',
            user_id : '',
            status : 0,
            visibleModal : true,
            image : null,
            loanData : null,
            cscore : null,
            accessToken : null
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor : '#FFFFFF'
            },
            headerTitle :( 
                <Image 
                    source={require('../../assets/logo.png')}
                    style={{ width: 120, height: 120, flex : 1, }}
                    resizeMode='contain'
                />
            ),
            headerTitleStyle:{
                alignSelf:'center',
            },
            headerLeft : null,
            gesturesEnabled: false,
        }
    };
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this._handleBackButton); 

        const { navigation } = this.props;
        const token = navigation.getParam('token');
        const key = navigation.getParam('key');
        const decoded = JWT.decode(token, key);

        this._storeData(decoded.access_token, String(decoded.user_id))
        this.setState({
            first_name : decoded.first_name,
            last_name : decoded.last_name,
            contact_no : decoded.contact,
            status : decoded.status,
            user_id : decoded.user_id,
            email : decoded.username,
            cscore : decoded.credit_score,
            image : decoded.image

        }, () => this._getLoanData());

    }
    _storeData = async (accessToken, userId) =>{
        try{
            await AsyncStorage.multiSet([['userToken', accessToken], ['userId', userId]]);
            await this._retrieveData();
        }catch(exception){
            console.log(exception)
        }
    }
    _retrieveData = async () =>{
        try{
            const data = await AsyncStorage.getItem('userToken');
            if(data != null){
                this.setState({ accessToken : data}, () => console.log(this.state.accessToken))
            }
        }catch(exception){
            console.log(exception);
        }
    }
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackButton);
    }

    _handleBackButton(){
        return true;
    }
    _getLoanData(){
        if(this.state.status != 0){
            Api._retrieveLoanData(data => {
                try{
                    console.log(data)
                    this.setState({ loanData : data.results});
                }catch(exception){
                    console.log(exception);
                }
            })  
        }
    }
    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });
    
        console.log(result);
    
        if (!result.cancelled) {
          this.setState({ image: result});
          console.log(this.state.image)
        }
    };
    _uploadImage = async () =>{
        let param = {
            image : this.state.image.uri,
            id : this.state.user_id
        };
        Api._uploadImage(param, (data)=> {
            try{
                console.log(data)
                this.setState({ image : null})
            }catch(exception){
                console.log(exception);
            }
        })
    };


    render(){
        let content;
        this.state.status == 0? (
            content = (
                <View style={styles.background}>
                    <Text style={style.infoOnColored}>Upgrade your account by confirming your identity.</Text>
                    <Text style={style.infoOnColored}>Upload a photo to confirm your identity</Text>
                    <View style={{justifyContent :'center', alignItems : 'center', textAlign : 'center', marginTop : 40}}>
                        {this.state.image == null ?  (<Text style={styles.centerUnderlineText}>No image chosen.</Text>)
                        :(<Text style={styles.centerUnderlineText}>{this.state.image.uri}</Text>)}
                        <Button 
                            onPress={this._pickImage}
                            title={'Choose image to upload'}
                            buttonStyle={styles.button}
                        />
                        <ThemeProvider>
                            <Button 
                                onPress={this._uploadImage}
                                title={'Upload image'}
                                titleStyle={{color : '#93d265'}}
                                buttonStyle={styles.buttonUpload}
                            />
                        </ThemeProvider>
                    </View>

                </View>
            )
        ):(
            content = (
                <React.Fragment>
                    <ScrollView style={styles.scrollViewStyle}>
                        <View style={styles.background}>
                            {this.state.cscore != null ? 
                                <CreditScoreCard 
                                    score={this.state.cscore} 
                                    image={this.state.image}
                                    user_name={this.state.first_name +" "+ this.state.last_name} 
                                    contact={this.state.contact_no} 
                                    email={this.state.email} 
                                /> 
                            : null}
                            <LoanCard 
                                data = {this.state.loanData} 
                                accessToken={this.state.accessToken} 
                                user_id={this.state.user_id} 
                                user_name={this.state.first_name +" "+ this.state.last_name} 
                                contact={this.state.contact_no} 
                                email={this.state.email}
                            />
                        </View>
                    </ScrollView>
                </React.Fragment>
            )
        )
        return (
            <React.Fragment>
                {content}
                <CustomFab 
                    id={this.state.user_id} 
                    firstName={this.state.first_name} 
                    lastName={this.state.last_name} 
                    contactNo={this.state.contact_no}
                />
            </React.Fragment>
        );
    }
}


const styles =  StyleSheet.create({
    background : {
        backgroundColor: '#f0f0f0',
        justifyContent : 'center',
        alignItems : 'center',
        width: '100%',
        height : '100%'
    },
    scrollViewStyle : {
        width : '100%',
    },
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 30,
        justifyContent : 'center',
        alignItems : 'center',
        textAlign : 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius : 20,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignItems : 'center'
    },
    modalContentSubtitle : {
        color : 'gray',
    },
    button : {
        backgroundColor : '#93d265',
        borderRadius : 20,
        height : 40,
        width : 250,
        marginBottom : 30
    },
    buttonUpload : {
        backgroundColor : '#FFFF',
        borderRadius : 20,
        height : 40,
        width : 250
    },
    centerUnderlineText : {
        textDecorationLine : 'underline', 
        color : 'white', 
        marginBottom : 30,
        textAlign: 'center'
    }

})


export default HomeScreen;