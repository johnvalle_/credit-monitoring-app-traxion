import React from 'react';
import { View, Text, Dimensions, StyleSheet, ScrollView, AsyncStorage, Image, TouchableOpacity } from 'react-native';
import { List, ListItem, Card, CardItem, Body, Icon } from 'native-base';
import Loader from '@components/Loader';
import Api from '@API/initialize';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class PaymentScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor : '#FFFFFF'
            },
            headerTitle :( 
                <Image 
                    source={require('../../assets/logo.png')}
                    style={{ width: 120, height: 120, flex : 1, }}
                    resizeMode='contain'
                />
            ),
            headerTitleStyle:{
                alignSelf:'center',
            },
            headerRight: <View style={{height: 50, width: 50}}></View>,
            gesturesEnabled: false,
        }
    }
    constructor(props){
        super(props);
        this.state = {
            user_id : '',
            access_token : '',
            paymentList : null,
            loading : true,
        }
    }
    async _retrieveInfo(){
        const token = await AsyncStorage.getItem('userToken');
        const id = await AsyncStorage.getItem('userId');
        this.setState({
            user_id : id,
            access_token : token
        }, () => this._getLoans())
    }
    _getLoans(){
        Api._getPaymentList(this.state.user_id, this.state.access_token, (data)=>{
            try{
                console.log(data)
                this.setState({
                    paymentList : data,
                    loading : false
                }, () => console.log(this.state.paymentList.length))
            }catch(exception){
                console.log(exception)
            }
        });
    }
    _handlePress(id){
        const loan_id = id;
        console.log(loan_id)
        this.props.navigation.navigate('PaymentDetails', {
            id : loan_id
        })
    }
    componentDidMount() {
        this._retrieveInfo();
    }
    render(){
        let data = this.state.paymentList;
        let res;
        if(!this.state.loading){
            res = data.map((payment) => 
                <Card key={payment.id} style={styles.cardStyle}>
                    <CardItem bordered>
                        <Body>
                            <Text style={styles.cardTitle}>{payment.item}</Text>
                            <Text style={styles.description}>
                                Start Date: 
                                <Text style={{fontWeight: 'bold'}}>
                                    &nbsp;{new Date(payment.created_at).toDateString()}
                                </Text> 
                            </Text>
                            <Text style={styles.description}>
                                Due Date:  
                                <Text style={{fontWeight: 'bold'}}>
                                    &nbsp;{new Date(payment.due_date).toDateString()}
                                </Text>.
                            </Text>
                            <Text style={styles.description}>Status: 
                                <Text style={{fontWeight: 'bold'}}>
                                    &nbsp;{(payment.status).charAt(0).toUpperCase()+(payment.status).slice(1)}
                                </Text>
                            </Text>
                        </Body>
                    </CardItem>
                    <CardItem bordered footer>
                        <TouchableOpacity onPress={() => this._handlePress(payment.id)} style={{flexDirection : 'row', flex : 1, justifyContent : 'flex-end'}}>
                            <Text style={{color : 'grey', marginTop : 5}}>Next payment dates</Text>
                            <Icon type="Feather" name="chevron-right" size={10} style={{color: 'grey', marginLeft : 10}} />
                        </TouchableOpacity>
                    </CardItem>
                </Card>
            );
        }else{
            res = <View style={{alignItems: 'center', justifyContent: 'center', marginTop : height*.3}}><Loader /></View>
        }
        return(
            <View style={{backgroundColor : '#f0f0f0'}}>
                <ScrollView>
                    {res}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardTitle : {
        color : '#5271ff',
        fontSize : 18,
        fontWeight : 'bold'
    },
    description : {
        color : 'grey',
    },
    cardStyle : {
        width : width*1,
        borderRadius : 20,
        elevation : 3,
        alignSelf : 'center'
    },
})
export default PaymentScreen;