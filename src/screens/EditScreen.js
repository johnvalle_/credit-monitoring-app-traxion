import React from 'react';
import { Text, View, KeyboardAvoidingView, Dimensions, StyleSheet, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import { Button, ThemeProvider, Divider } from 'react-native-elements';
import { Item, Input, Icon } from 'native-base';
import Api from '@API/initialize';
import Modal from 'react-native-modal';
import style from '@assets/styles/style';
import Loader from '@components/Loader';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class RegisterScreen extends React.Component {
    _isMounted = false;
    constructor(props){
        super(props);
        this.state = {
            first_name : '',
            last_name : '',
            email : '',
            contact_no : '',
            password : '',
            visibleModal : false,
            modalToShow : 0,
            viewPass : false,
            loading : true
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor : '#FFFFFF'
            },
            headerTitle :( 
                <Image 
                    source={require('../../assets/logo.png')}
                    style={{ width: 120, height: 120, flex : 1, }}
                    resizeMode='contain'
                />
            ),
            headerTitleStyle:{
                alignSelf:'center',
            },
            headerRight: <View style={{height: 50, width: 50}}></View>,
            gesturesEnabled: false,
        }
    }
    async _handleSave(){
        if(this.state.first_name == null || this.state.last_name == null || this.state.contact_no == null){
            this.setState({
                visibleModal : true,
                modalToShow : 'empty'
            })
        }else{
            try{
                const token = await AsyncStorage.getItem('userToken');
                var params = {
                    first_name : this.state.first_name,
                    last_name : this.state.last_name,
                    contact_no : this.state.contact_no
                }
                console.log(this.state.user_id)
                Api._update(this.state.user_id, token, params, (data) => {
                    try{
                        console.log(data)
                        this._getData();
                        this.setState({ visibleModal : true, modalToShow : 'update'})
                    }catch(exception){
                        console.log(exception);
                    }
                })
                
            }catch(exception){
                console.log(exception);
            }
        }
    }
    _handleCancel(){
        this.props.navigation.navigate('Home');
    }
    async _getData(){
        const { navigation } = this.props;
        const token = await AsyncStorage.getItem('userToken');
        const id = navigation.getParam('userId');
        this.setState({ loading : true },() => {
            Api._getProfile(id, token, (data)=> {
                try{
                    console.log(data)
                    this.setState({
                        first_name : data.first_name,
                        last_name : data.last_name,
                        contact_no : data.contact_no,
                        loading : false,
                        user_id : data.id
                    })
                }catch(exception){
                    console.log(exception)
                }
            })
        })
        
    }
    componentDidMount(){
        this._isMounted = true;
        if(this._isMounted){
            this._getData();
        }
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    render() {
        let res;
        this.state.loading ? (
            res =<View style={{alignItems: 'center', justifyContent: 'center'}}><Loader /></View>
        ):(
            res = (
                <React.Fragment>
                    <KeyboardAvoidingView behavior="padding" enabled>
                    <View style={styles.container}>
                        <Text style={{ color : 'grey', fontSize : 24, marginBottom : 30, marginLeft : 10, textAlign : 'center', justifyContent : 'center', alignSelf : 'center'}}>Edit Profile</Text>
                            
                            <Text style={styles.title}>First Name</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your first name'
                                    onChangeText = {(first_name) => {this.setState({first_name})}}
                                    value = {this.state.first_name}
                                    placeholderTextColor = 'gray'  
                                />
                            </Item>
                            <Text style={styles.title}>Last Name</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your last name'
                                    onChangeText = {(last_name) => {this.setState({last_name})}}
                                    value = {this.state.last_name}
                                    placeholderTextColor = 'gray'  
                                />
                            </Item>
                            <Text style={styles.title}>Mobile Number</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your contact number'
                                    onChangeText = {(contact_no) => {this.setState({contact_no})}}
                                    value = {this.state.contact_no}
                                    placeholderTextColor = 'gray'  
                                    keyboardType='phone-pad'
                                />
                            </Item>
                            <Button 
                                type="solid"
                                onPress={() => {this._handleSave()}}
                                title="Save"
                                buttonStyle={styles.button}
                                raised
                            />
                            <ThemeProvider>
                                <Button 
                                    type="outline"
                                    onPress={() => {this._handleCancel()}}
                                    title="Cancel"
                                    buttonStyle={styles.buttonClear}
                                    titleStyle = {{color : '#5271FF'}}
                                />
                            </ThemeProvider>
                    </View>
                </KeyboardAvoidingView>
                <Modal
                    isVisible ={this.state.visibleModal} 
                    style={styles.bottomModal}
                    onBackdropPress={() => this.setState({ visibleModal : false})}
                    >
                    <View style={styles.modalContent}>
                    {this.state.modalToShow == 'empty' ? (
                        <View>
                            <Text style={[styles.modalContentTitle, style.warning]}>Uh oh. Some fields are empty.</Text>
                            <Text style={styles.modalContentSubtitle}>Please fill all fields with information.</Text>
                        </View>
                    ): this.state.modalToShow == 'success' ? (
                        <View>
                            <Text style={[styles.modalContentTitle, style.success]}>Successfully registered.</Text>
                            <Text style={styles.modalContentSubtitle}>Welcome to Cremon!. Please login.</Text>
                        </View>
                    ): this.state.modalToShow == 'update' ?(
                        <View>
                            <Text style={[styles.modalContentTitle, style.success]}>Changes saved.</Text>
                            <Text style={styles.modalContentSubtitle}>You have successfully edited your profile.</Text>
                        </View>
                    ):(
                        <View>
                            <Text style={[styles.modalContentTitle, style.danger]}>Registration Failed.</Text>
                            <Text style={styles.modalContentSubtitle}>You already registered. Please login.</Text>
                        </View>
                    )}
                    </View>
                </Modal>   
            </React.Fragment>
            )
        );
        return (
            <View style={styles.background}>
               {res}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    background : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#FFFF',
        width: '100%',
        height : '100%'
    },
    container : {
        // flex : 1,
        marginTop : 10,
        width : width*.8
    },
    input : {
        height : 40, 
        borderWidth : .25,
        borderColor : 'grey',
        width : width * 0.8,
        marginTop : 5,
        marginBottom : 20,
        padding : 5,
        borderRadius : 5
    },
    title : {
        fontWeight : 'bold',
        color : '#5271FF',
        marginBottom : 5
    },
    button : {
        backgroundColor : '#5271FF',
        borderRadius : 20,
        height : 40,
    },
    buttonClear : {
        borderRadius : 20,
        marginTop : 10,
        height : 40,
        borderColor : '#5271FF',  
    },
    inputText : {
        color : 'grey'
    },
    image : {
        width: 150, 
        height: 150, 
        marginBottom : 10, 
    },
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 22,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignSelf : 'center'
    },
    modalContentSubtitle : {
        color : 'gray'
    }
    

})

export default RegisterScreen;