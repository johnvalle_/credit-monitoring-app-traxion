import React from 'react';
import { View, Text, Dimensions, StyleSheet, ScrollView, AsyncStorage, Image, TouchableOpacity } from 'react-native';
import { List, ListItem, Card, CardItem, Body, Icon } from 'native-base';
import Loader from '@components/Loader';
import Api from '@API/initialize';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class PaymentDetailsScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor : '#FFFFFF'
            },
            headerTitle :( 
                <Image 
                    source={require('../../assets/logo.png')}
                    style={{ width: 120, height: 120, flex : 1, }}
                    resizeMode='contain'
                />
            ),
            headerTitleStyle:{
                alignSelf:'center',
            },
            headerRight: <View style={{height: 50, width: 50}}></View>,
            gesturesEnabled: false,
        }
    }
    constructor(props){
        super(props);
        this.state = {
            userId : null,
            access_token : null,
            paymentDetails : null,
            loading : false,
        }
    }
    async _retrieveInfo(){
        const token = await AsyncStorage.getItem('userToken');
        const id = await AsyncStorage.getItem('userId');
        const { navigation } = this.props;
        const loan_id = navigation.getParam('id');
        this.setState({
            user_id : id,
            access_token : token
        }, () => this._getDetails(loan_id))
    }
    _getDetails(id){
        const loan_id = id;
        Api._getPaymentDetails(loan_id, this.state.access_token, (data)=>{
            try{
                console.log(data);
                this.setState({
                    paymentDetails : data,
                    loading : false
                })
            }catch(exception){
                console.log(exception);
            }
        })
    }
    componentDidMount() {
        this._retrieveInfo();
    }
    render(){
        let res;
        let data = this.state.paymentDetails;
        if(!this.state.loading && this.state.paymentDetails != null){
            
            let details = data.map((detail) => 
                <ListItem noIndent style={(detail.id%2) ? {backgroundColor : '#f0f0f0'} : {backgroundColor : '#FFFFFF'}} key={detail.id}>
                    <Text>{new Date(detail.date).toDateString()}</Text>
                    <View style={{flexDirection : 'row', flex : 1, justifyContent : 'flex-end'}}>
                        <Icon type="FontAwesome" name={detail.status == 0 ? "times-circle" : "check-circle"} size={10} style={detail.status == 0 ?{color: 'grey', marginRight : 20}:{color: '#93d265', marginRight : 20}} />
                    </View>
                </ListItem>

            );
            res = (
                <ScrollView>
                    <List>
                        <ListItem>
                            <Text>Amount to pay: PHP&nbsp;<Text style={{fontWeight: 'bold', fontSize: 15}}>{data[0].amount}</Text></Text>
                        </ListItem>
                        {details}
                    </List>
                </ScrollView>
            ) 
        }else{
            res = <View style={{alignItems: 'center', justifyContent: 'center', marginTop : height*.3}}><Loader /></View>
        }
        return res;
    }
}

export default PaymentDetailsScreen;