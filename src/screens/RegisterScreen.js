import React from 'react';
import { Text, View, KeyboardAvoidingView, Dimensions, StyleSheet, TouchableOpacity} from 'react-native';
import { Button, Image, ThemeProvider, Divider } from 'react-native-elements';
import { Item, Input, Icon } from 'native-base';
import Api from '@API/initialize';
import Modal from 'react-native-modal';
import style from '@assets/styles/style';

const width = Dimensions.get('window').width;

class RegisterScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            first_name : '',
            last_name : '',
            email : '',
            contact_no : '',
            password : '',
            visibleModal : false,
            modalToShow : 0,
            viewPass : false,
        }
    }
    static navigationOptions = {
        title: 'Register',
        header: null,
        gesturesEnabled: false,
    };
    _handleRegister(){
        var params = {
            "email" : this.state.email,
            'password' :  this.state.password,
            "first_name" : this.state.first_name,
            "last_name" : this.state.last_name,
            "contact_no" : this.state.contact_no
        }
        // Field check if sending null fields
        if(!this.state.email || !this.state.password || !this.state.first_name || !this.state.last_name || !this.state.contact_no){
            console.log('has null');
            this.setState({ visibleModal : true, modalToShow: 'empty'})
            
        }else{
            Api._register(params, (data) => {
                try{
                    if("error" in data){
                        this.setState({ visibleModal : true, modalToShow : 'registered'})
                    }else{
                        this.setState({
                            first_name : '',
                            last_name : '',
                            email : '',
                            contact_no : '',
                            password : ''
                        });
                        this.setState({ visibleModal : true, modalToShow : 'success'})
                        console.log(data)
                    }
                }catch(exception){
                    console.log(exception);
                }
            })
        }

        console.log(params)
    }
    _handleLogin(){
        this.props.navigation.navigate('Login');
    }
    _handleEyeToggle(){
        this.setState({ viewPass : !this.state.viewPass})
    }
    render() {
        return (
            <View style={styles.background}>
                <KeyboardAvoidingView behavior="padding" enabled>
                    <View style={styles.container}>
                        <Text style={{ color : 'grey', fontSize : 24, marginBottom : 30, marginLeft : 10, textAlign : 'center', justifyContent : 'center', alignSelf : 'center'}}>Register to Cremon</Text>
                            
                            <Text style={styles.title}>First Name</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your first name'
                                    onChangeText = {(first_name) => {this.setState({first_name})}}
                                    value = {this.state.first_name}
                                    placeholderTextColor = 'gray'  
                                />
                            </Item>
                            <Text style={styles.title}>Last Name</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your last name'
                                    onChangeText = {(last_name) => {this.setState({last_name})}}
                                    value = {this.state.last_name}
                                    placeholderTextColor = 'gray'  
                                />
                            </Item>
                            <Text style={styles.title}>Email Address</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your email address'
                                    onChangeText = {(email) => {this.setState({email})}}
                                    value = {this.state.email}
                                    placeholderTextColor = 'gray'  
                                    keyboardType='email-address'
                                />
                            </Item>
                            <Text style={styles.title}>Password</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your password'
                                    onChangeText = {(password) => {this.setState({password})}}
                                    value = {this.state.password}
                                    placeholderTextColor = 'gray'  
                                    secureTextEntry={!this.state.viewPass}
                                />
                                <TouchableOpacity onPress={() => this._handleEyeToggle()}>
                                    <Icon
                                        type="Feather"
                                        name={this.state.viewPass == true? 'eye' : 'eye-off'}
                                        color='grey'
                                        style={{fontSize:14 ,marginRight : 10}}
                                    />
                                </TouchableOpacity>
                            </Item>
                            <Text style={styles.title}>Mobile Number</Text>
                            <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                                <Input
                                    placeholder='Enter your contact number'
                                    onChangeText = {(contact_no) => {this.setState({contact_no})}}
                                    value = {this.state.contact_no}
                                    placeholderTextColor = 'gray'  
                                    keyboardType='phone-pad'
                                />
                            </Item>
                            <Button 
                                type="solid"
                                onPress={() => {this._handleRegister()}}
                                title="Register"
                                buttonStyle={styles.button}
                                raised
                            />
                            <ThemeProvider>
                                <Button 
                                    type="outline"
                                    onPress={() => {this._handleLogin()}}
                                    title="Login"
                                    buttonStyle={styles.buttonClear}
                                    titleStyle = {{color : '#5271FF'}}
                                />
                            </ThemeProvider>
                    </View>
                </KeyboardAvoidingView>
                <Modal
                    isVisible ={this.state.visibleModal} 
                    style={styles.bottomModal}
                    onBackdropPress={() => this.setState({ visibleModal : false})}
                    >
                <View style={styles.modalContent}>
                {this.state.modalToShow == 'empty' ? (
                    <View>
                        <Text style={[styles.modalContentTitle, style.warning]}>Uh oh. Some fields are empty.</Text>
                        <Text style={styles.modalContentSubtitle}>Please fill all fields with information.</Text>
                    </View>
                ): this.state.modalToShow == 'success' ? (
                    <View>
                        <Text style={[styles.modalContentTitle, style.success]}>Successfully registered.</Text>
                        <Text style={styles.modalContentSubtitle}>Welcome to Cremon!. Please login.</Text>
                    </View>
                ):(
                    <View>
                        <Text style={[styles.modalContentTitle, style.danger]}>Registration Failed.</Text>
                        <Text style={styles.modalContentSubtitle}>You already registered. Please login.</Text>
                    </View>
                )}
                </View>
            </Modal>   

            </View>
        );
    }
}

const styles = StyleSheet.create({
    background : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#FFFF',
        width: '100%',
        height : '100%'
    },
    container : {
        // flex : 1,
        marginTop : 10,
        width : width*.8
    },
    input : {
        height : 40, 
        borderWidth : .25,
        borderColor : 'grey',
        width : width * 0.8,
        marginTop : 5,
        marginBottom : 20,
        padding : 5,
        borderRadius : 5
    },
    title : {
        fontWeight : 'bold',
        color : '#5271FF',
        marginBottom : 5
    },
    button : {
        backgroundColor : '#5271FF',
        borderRadius : 20,
        height : 40,
    },
    buttonClear : {
        borderRadius : 20,
        marginTop : 10,
        height : 40,
        borderColor : '#5271FF',  
    },
    inputText : {
        color : 'grey'
    },
    image : {
        width: 150, 
        height: 150, 
        marginBottom : 10, 
    },
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 22,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignSelf : 'center'
    },
    modalContentSubtitle : {
        color : 'gray'
    }
    

})

export default RegisterScreen;