import React from 'react';
import { Text, Button, Icon, ThemeProvider, Image } from 'react-native-elements';
import { BackHandler, AsyncStorage, StyleSheet, View, Dimensions, TouchableOpacity, Platform, ScrollView } from 'react-native';
import { ListItem, Radio, Right, Left} from 'native-base';
import { Card, CardTitle, CardAction, CardContent, CardButton,} from 'react-native-material-cards';
import ApplyLoanCard from '@components/ApplyLoanCard';

const width = Dimensions.get('window').width;

class ApplyLoanScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }
    static navigationOptions = {
        headerStyle: {
            backgroundColor : '#FFFFFF'
        },
        headerTitleStyle:{
            alignSelf:'center',
        },
        headerRight: <View style={{height: 50, width: 50}}></View>,
        gesturesEnabled: false,
    };
    render(){
        const { navigation } = this.props;
        const item_id = navigation.getParam('item_id');
        const desc = navigation.getParam('description');
        const price = navigation.getParam('price');
        const name = navigation.getParam('name');
        const accessToken = navigation.getParam('accessToken');
        const user_data = {
            ...user_data,
            id : navigation.getParam('user_id'),
            name : navigation.getParam('user_name'),
            email : navigation.getParam('email'),
            contact : navigation.getParam('contact')
        }

        return(
            <ApplyLoanCard item_id={item_id} userId={user_data.id} desc={desc} price={price} name={name} accessToken={accessToken}/>
        )
    }
}


const styles =  StyleSheet.create({
    background : {
        backgroundColor: '#FFFF',
        justifyContent : 'center',
        alignItems : 'center',
    },
})


export default ApplyLoanScreen;