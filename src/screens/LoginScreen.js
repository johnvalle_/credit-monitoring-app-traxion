import React from 'react';
import { Text, View, KeyboardAvoidingView, Dimensions, StyleSheet, TouchableOpacity} from 'react-native';
import { Input, Item, Icon } from 'native-base'
import { Button, Image, ThemeProvider } from 'react-native-elements';
import Api from '@API/initialize';
import {Card, CardTitle, CardAction, CardContent, CardButton} from 'react-native-material-cards';
import Modal from 'react-native-modal';
import style from '@assets/styles/style';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class LoginScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username : '',
            password : '',
            visibleModal : false,
            modalToShow : '',
            viewPass : false,
        }
    }
    static navigationOptions = {
        title: 'Login',
        header: null,
        gesturesEnabled: false,
    };
    _handleLogin(){
        var params = {
            username : this.state.username,
            password : this.state.password
        }
        if(!this.state.username || !this.state.password){
            this.setState({ visibleModal : true, modalToShow : 'empty'})
        }else{
            Api._login(params, (data) =>{
                try{
                    console.log(data)
                    if("error" in data || data == "Account doesn't exist."){
                        this.setState({ visibleModal : true, modalToShow : 'error'})
                    }else{
                        let token = data.user;
                        let key = 'gwapo';
                        this.props.navigation.navigate('Home', {
                            token : token,
                            key : key
                        })
                    }
                }catch(exception){
                    console.log(exception);
                }
            });
        }
    }
    _handleBack(){
        this.props.navigation.navigate('Register');
    }
    _handleEyeToggle(){
        this.setState({ viewPass : !this.state.viewPass})
    }
    componentDidMount() {
        const { navigation } = this.props;
        const password = navigation.getParam('password');
        const username = navigation.getParam('email');

        username != null && password != null ? (
            this.setState({
                username : username,
                password : password
            })
        ):(
            null
        )
    }
    render() {

        return (
            <View style={styles.background}>
                                <KeyboardAvoidingView behavior="padding" enabled>
                    <Image
                        source={require('../assets/cremon_logo.png')} 
                        style={styles.image}
                    />
                    <View style={styles.container}>
                        <Text style={styles.title}>Username</Text>
                        <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                            <Icon
                                type="Feather"
                                name='user'
                                size={12}
                                color='#5271ff'
                                style={{fontSize : 14, marginLeft : 10}}
                            />
                            <Input
                                placeholder='Enter your username'
                                onChangeText = {(username) => {this.setState({username})}}
                                value = {this.state.username}
                                placeholderTextColor = 'gray'  
                            />
                        </Item>
                        <Text style={styles.title}>Password</Text>
                        <Item regular style={{marginBottom : 20, borderRadius : 5}}>
                            <Icon
                                type="Feather"
                                name='lock'
                                color='#5271ff'
                                style={{fontSize: 14, marginLeft : 10}}
                            />
                            <Input
                                placeholder='Enter your password'
                                onChangeText = {(password) => {this.setState({password})}}
                                value = {this.state.password}
                                placeholderTextColor = 'gray' 
                                secureTextEntry ={!this.state.viewPass}   
                            />
                            <TouchableOpacity onPress={() => this._handleEyeToggle()}>
                                <Icon
                                    type="Feather"
                                    name={this.state.viewPass == true? 'eye' : 'eye-off'}
                                    color='grey'
                                    style={{fontSize: 14,marginRight : 10}}
                                />
                            </TouchableOpacity>
                        </Item>
                        <Button 
                            type="solid"
                            onPress={() => {this._handleLogin()}}
                            title="Login"
                            buttonStyle={styles.button}
                            raised
                        />
                        <ThemeProvider>
                            <Button
                                type = "clear"
                                icon={
                                    <Icon
                                        type="Feather"
                                        name="chevron-left"
                                        color="#5271ff"
                                        style = {{fontSize: 14, marginRight : 10}}
                                    />
                                }
                                title="No account yet? Signup."
                                buttonStyle = {{marginTop : 20}}
                                onPress = {() => {this._handleBack()}}
                                titleStyle = {{color : '#5271ff', textDecorationLine : 'underline'}}
                            />
                        </ThemeProvider>
                    </View>
                </KeyboardAvoidingView>
                <Modal
                    isVisible ={this.state.visibleModal} 
                    style={styles.bottomModal}
                    onBackdropPress={() => this.setState({ visibleModal : false})}
                >
                    <View style={styles.modalContent}>
                    {this.state.modalToShow == 'empty' ? (
                        <View>
                            <Text style={[styles.modalContentTitle, style.warning]}>Uh oh. Some fields are empty.</Text>
                            <Text style={styles.modalContentSubtitle}>Please fill all fields with information.</Text>
                        </View>
                    ) : (
                        <View>
                            <Text style={[styles.modalContentTitle, style.danger]}>Login Failed.</Text>
                            <Text style={styles.modalContentSubtitle}>Wrong credentials.</Text>
                        </View>
                    )}
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    background : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : '#FFFF',
        width: '100%',
        height : '100%'
    },
    container : {
        // flex : 1,
        width : width*.8,
        marginTop : 30
    },
    title : {
        fontWeight : 'bold',
        color : '#4C78F9',
        marginBottom : 5
    },
    header : {
        fontSize : 24,
        fontWeight : 'bold',
        justifyContent : 'center',
        alignSelf : 'center',
        marginTop: 20,
        marginBottom: 20,
        color : '#4C78F9'
    },
    button : {
        backgroundColor : '#5271FF',
        borderRadius : 25,
        height : 50,
    },
    image : {
        width: 250, 
        height: 250, 
        marginBottom : 10, 
        justifyContent : 'center', 
        alignSelf : 'center' 
    },
    bottomModal : {
        justifyContent : 'flex-end',
        margin : 0
    },
    modalContent : {
        backgroundColor : 'white',
        padding : 22,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 5,
        borderColor : 'rgba(0,0,0,0.1)'
    },
    modalContentTitle : {
        fontWeight : 'bold',
        marginLeft : 10,
        justifyContent : 'center',
        alignSelf : 'center'
    },
    modalContentSubtitle : {
        color : 'gray'
    }
    
    
})

export default LoginScreen;