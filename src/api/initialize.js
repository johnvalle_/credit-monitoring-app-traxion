import React from 'react';

class Endpoints extends React.Component {

    API_URL = 'http://10.199.1.228:8000/api';

    _API_POST = async (source, data, callback) => {
        try {
            let response = await fetch(source, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            });
            let responseJson = await response.json();
            callback(responseJson);
        } catch (exception) {
            console.log(exception)
        }
    }
    _API_PUT = async (source, data, callback) => {
        fetch(source, {
                method: "PUT",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Content-Type': 'multipart/form-data',
                },
                body: data,
            })
            .then(response => response.json())
            .then(response => {
                callback(response)
            })
            .catch(error => {
                console.log(error);
            });
    }
    _API_GET = (source, callback) => {

        fetch(source, {
                method: 'GET',
            })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });


    }
    _login = (param, callback) => {
        this._API_POST(this.API_URL + '/login/form/',
            param,
            (data) => {
                callback(data);
            }
        )
    }
    _register = (param, callback) => {
        this._API_POST(this.API_URL + '/registration/form/',
            param,
            (data) => {
                callback(data);
            }
        )
    }

    _retrieveLoanData = (callback) => {
        this._API_GET(this.API_URL + '/loans/',
            (data) => {
                callback(data);
            }
        )
    }

    _uploadImage = (param, callback) => {
        let localUri = param.image;
        let filename = localUri.split('/').pop();

        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        let formData = new FormData();
        formData.append('image', {
            uri: localUri,
            name: filename,
            type
        });
        formData.append('username', param.email);
        this._API_PUT(this.API_URL + '/image/upload/',
            formData,
            (data) => {
                callback(data);
            }
        )

    }

    _transact = (token, param, callback) => {
        this._API_POST(this.API_URL + '/transact/make-transaction/?access_token='+token,
            param,
            (data) => {
                callback(data);
            }
        )
    }

    _update = async (id, token, param, callback) => {
        let url = this.API_URL + '/user/'+id+'/update/?access_token='+token;
        console.log(JSON.stringify(param))
        try {
            let response = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(param),
            });
            let responseJson = await response.json();
            console.log(responseJson)
            callback(responseJson);
        } catch (exception) {
            console.log(exception)
        }
    }

    _getProfile = async(id, token, callback) => {
        let url = this.API_URL + '/user/'+id+'/?access_token='+token;
        console.log(url)
        try {
            let response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            let responseJson = await response.json();
            console.log(responseJson)
            callback(responseJson);
        } catch (exception) {
            console.log(exception)
        }
    }

    _getPaymentList = async (id, token, callback) => {
        let url = this.API_URL + '/user/' +id+'/user-transactions/?access_token='+token;
        console.log(url)
        try{
            let response = await fetch(url, {
                method : 'GET',
                headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            let responseJson = await response.json();
            callback(responseJson);
        }catch(exception){
            console.log(exception);
        }
    }
    _getPaymentDetails = async (id, token, callback) => {
        let url = this.API_URL + '/loan-transactions/' +id+ '/transaction-details/?access_token='+token;
        console.log(url);
        try{
            let response = await fetch(url, {
                method : 'GET',
                headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            });
            let responseJson = await response.json();
            callback(responseJson);
        }catch(exception){
            console.log(exception);
        }
    }
}

const Api = new Endpoints();

export default Api;

// /loan-transactions/{id}/
// /loan-transactions/{id}/transaction-details/?access_token
// /user/{useraccountid}/user-transactions/?acess_token
// /user/{id}/update